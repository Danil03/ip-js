document.getElementById('find-ip-btn').addEventListener('click', async function() {
    try {
        const ipResponse = await fetch('https://api.ipify.org/?format=json');
        const { ip } = await ipResponse.json();
        const ipDetailsResponse = await fetch(`https://ip-api.com/json/${ip}`);
        const ipDetails = await ipDetailsResponse.json();

        const ipInfoElement = document.getElementById('ip-info');
        ipInfoElement.innerHTML = `
            <p>Континент: ${ipDetails.continent}</p>
            <p>Країна: ${ipDetails.country}</p>
            <p>Регіон: ${ipDetails.regionName}</p>
            <p>Місто: ${ipDetails.city}</p>
            <p>Район: ${ipDetails.district}</p>
        `;
    } catch (error) {
        console.error('Помилка:', error);
    }
});

// Коли виконується асинхронний код, JavaScript продовжує виконання інших операцій, а не чекає, доки цей код завершиться.